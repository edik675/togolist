@extends('layouts.app')
<link href="{{asset('css/to_go_list.css')}}" rel="stylesheet">
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">

        </div>

        <div class="col-md-3">
            <label class="checkbox-inline">
                <input id="allow_adding_places" type="checkbox" value="1">
                Allow adding places
            </label>
        </div>

        <div class="col-md-1 text-right">
            <button class="btn btn-xs btn-primary" title="help" data-toggle="modal" data-target="#helpModal">?</button>
        </div>
    </div>

    <div class="row m-top-10">
        <div class="col-md-8">
            <div id="map"></div>

            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div id="places_list">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="placeModal" tabindex="-1" role="dialog" aria-labelledby="placeModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <label for="place_name">Place name</label>
                <input type="text" class="form-control input-sm" id="place_name" placeholder="Place name" maxlength="90">
                <input type="hidden" id="id_place">
                <input type="hidden" id="place_lat">
                <input type="hidden" id="place_lng">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" id="save_place">Save</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div>
                    <img src="" id="new_places_icon">
                    - new place

                    <img src="" id="visited_places_icon">
                    - visited place
                </div>
                <hr>
                <div>
                    <ul>
                        <li>set checked <b>Allow adding places</b> and add places by clicking on the map</li>
                        <li>easily change coordinates by dragging markers on the map</li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://maps.googleapis.com/maps/api/js?v=3&sensor=true&libraries=geometry&key=AIzaSyCKmRWjGoe-pSKN7-aaI35rE7kFZM960-0&language=en"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{asset('js/to_go_list.js')}}"></script>
