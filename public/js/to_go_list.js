function ToGoList() {
    this.map = null;
    this.default_lat = 40.177610;
    this.default_lng = 44.512568;
    this.default_zoom = 8;
    this.markers = {};
    this.infowindow = null;
    this.infowindows = {};
    this.places = null;
    this.active_marker = null;
    this.last_active_marker = null;

    this.modal = {
        place: $('#placeModal')
    };

    this.form = {
        id_place: $("#id_place"),
        place_name: $("#place_name"),
        save_place: $("#save_place"),
        place_lat: $("#place_lat"),
        place_lng: $("#place_lng"),
        allow_adding_places: $("#allow_adding_places")
    };

    this.elem =  {
        places_list: $("div#places_list"),
        new_places_icon: $("#new_places_icon"),
        visited_places_icon: $("#visited_places_icon"),
    };

    this.marker_color = {
        simple: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
        visited: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
    };

    var obj = this;
    obj.init();
}

ToGoList.prototype.init = function() {
    var obj = this;

    obj.initMap();
    obj.initHelp();
};

// init map and add listeners
ToGoList.prototype.initMap = function() {
    var obj = this;

    obj.map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: obj.default_lat,
            lng: obj.default_lng
        },
        zoom: obj.default_zoom
    });

    obj.getPlaces();
    obj.initActions();
    obj.initClickOnList();
    obj.map.addListener('click', function(event) {
        obj.beforeAddMarker(event.latLng);
    });
};

ToGoList.prototype.beforeAddMarker = function(latlng) {
    var obj = this;

    var checked = obj.form.allow_adding_places.is(":checked") ? 1 : 0;
    if(!checked) {
        return false;
    }

    obj.resetModalFormInfo();
    obj.form.place_lat.val(latlng.lat());
    obj.form.place_lng.val(latlng.lng());
    obj.modal.place.modal('show');
};

ToGoList.prototype.addMarker = function (place) {
    var obj = this;

    var location = new google.maps.LatLng(place.latitude, place.longitude);

    var _name = place.name ? place.name : "no name";
    var _checked = place.visited ? 'checked' : '';
    var contentString = '' +
        '<label class="checkbox-inline"><input class="visited_place" type="checkbox" value="1" ' + _checked + ' data-id="' + place.id + '">visited</label>' +
        '<div id="place_content' + place.id + '">' +
        '<h3 class="name">' + _name + '</h3>' +
        '<div><small>added: ' + place.created_at + '</small></div>' +
        '<br>' +
        '<a href="#" class="alert-success edit_place" data-id="' + place.id + '">edit</a>' +
        ' | ' +
        '<a href="#" class="alert-danger delete_place" data-id="' + place.id + '">delete</a>' +
        '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString,
        // maxWidth: 300
    });

    obj.infowindows[place.id] = infowindow;

    google.maps.event.addListener(infowindow, 'closeclick',function(){
        obj.initActiveMarker(true);
    });

    var marker = new google.maps.Marker({
        position: location,
        draggable: true,
        map: obj.map,
        id: place.id
    });
    obj.markers[place.id] = marker;

    obj.setMarkerColor(place.id, place.visited);

    marker.addListener('click', function() {
        if (obj.infowindow) {
            obj.infowindow.close();
        }

        infowindow.open(obj.map, marker);
        obj.infowindow = infowindow;

        if(obj.active_marker) {
            obj.last_active_marker = obj.active_marker;
        }
        obj.active_marker = marker.id;
        obj.initActiveMarker();
    });

    google.maps.event.addListener(marker, 'dragend', function(event) {
        var data = [];
        data.id = this.id;
        data.lat = event.latLng.lat();
        data.lng = event.latLng.lng();

        obj.savePlace(data);
    });
};

// get user places list
ToGoList.prototype.getPlaces = function () {
    var obj = this;

    $.ajax({
        type: "get",
        url: '/places/get',
        data: {},
        success: function(data) {
            if(data.success == 1) {
                obj.putPlacesOnMap(data.places);
                obj.elem.places_list.html(data.places_list);
            }
        }
    });
};

ToGoList.prototype.putPlacesOnMap = function (places) {
    var obj = this;

    if(places.length < 1) {
        return false;
    }

    obj.places = places;
    $.each(places, function(index, place) {
        obj.addMarker(place);
    })
};

ToGoList.prototype.savePlace = function (info) {
    var obj = this;

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/place/add',
        data: {
            'id' : info.id,
            'lat' : info.lat,
            'lng' : info.lng,
            'name' : info.name
        },
        success: function(data) {
            obj.modal.place.modal('hide');
            if(data.place_info) {
                if(info.id == '') {
                    obj.addMarker(data.place_info);

                    if(data.html) {
                        obj.elem.places_list.find("div.list-group").prepend(data.html);
                    }
                }
                else {
                    if(info.name != '') {
                        obj.elem.places_list.find("a.place_in_list" + data.place_info.id).html(data.place_info.name);
                    }
                }

                obj.afterSavePlace(data.place_info);
            }
        }
    });
};

ToGoList.prototype.afterSavePlace = function(place_info) {
    var obj = this;

    obj.places[place_info.id] = place_info;

    var _content = $("div#place_content" + place_info.id);
    _content.find("h3.name").html(place_info.name ? place_info.name : 'no name');
};

ToGoList.prototype.initActions = function() {
    var obj = this;

    // save place name pressing enter when popup is opened
    obj.form.place_name.keypress(function (e) {
        if (e.which == 13) {
            obj.form.save_place.click();
            return false;
        }
    });

    // save place
    obj.form.save_place.on('click', function() {
        var info = {};
        info.id = obj.form.id_place.val();
        info.name = obj.form.place_name.val();

        if(info.id == '') {
            info.lat = obj.form.place_lat.val();
            info.lng = obj.form.place_lng.val();

            if(info.lat == '' || info.lng == '') {
                return false;
            }
        }

        obj.savePlace(info);
    });

    // place delete listener
    $(document).on('click', 'a.delete_place', function(e) {
        e.preventDefault();

        var $this = $(this);
        var id = $this.data('id');

        obj.deletePlace(id);
    });

    // place edit listener
    $(document).on('click', 'a.edit_place', function(e) {
        e.preventDefault();

        var $this = $(this);
        var id = $this.data('id');

        obj.editPlace(id);
    });

    // set as visited listener
    $(document).on('change', 'input.visited_place', function() {
        var $this = $(this);

        var id = $this.data('id');
        var checked = $this.is(":checked") ? 1 : 0;

        obj.setPlaceVisited(id, checked);
    });

    // focus on name input, when popup is open
    obj.modal.place.on('shown.bs.modal', function(e){
        obj.form.place_name.focus();
    });
};

ToGoList.prototype.deletePlace = function(id) {
    var obj = this;

    var cnf = confirm('Are you sure?');
    if(!cnf) {
        return true;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/place/delete',
        data: {
            'id' : id
        },
        success: function(data) {
            if(data.success == 1) {
                obj.last_active_marker = null;
                obj.active_marker = null;
                obj.initActiveMarker(true);

                obj.markers[id].setMap(null);
                delete obj.markers[id];
                delete obj.infowindows[id];

                obj.elem.places_list.find("a.place_in_list" + id).remove();
            }
        }
    });
};

ToGoList.prototype.editPlace = function(id) {
    var obj = this;

    if(!obj.places || !obj.places[id]){
        return false;
    }

    var place_info = obj.places[id];


    obj.resetModalFormInfo();
    obj.form.place_name.val(place_info.name);
    obj.form.id_place.val(place_info.id);
    obj.modal.place.modal('show');
};

ToGoList.prototype.resetModalFormInfo = function() {
    var obj = this;

    obj.form.place_name.val('');
    obj.form.id_place.val('');
    obj.form.place_lat.val('');
    obj.form.place_lng.val('');
};

ToGoList.prototype.initActiveMarker = function(no_active_marker, from_list) {
    var obj = this;

    if(obj.last_active_marker) {
        obj.infowindows[obj.last_active_marker].close(obj.map, obj.markers[obj.last_active_marker]);
        obj.elem.places_list.find('a.place_in_list' + obj.last_active_marker).removeClass('active');
        obj.last_active_marker = null;
    }

    if(obj.active_marker) {
        if(from_list) {
            obj.infowindows[obj.active_marker].open(obj.map, obj.markers[obj.active_marker]);
            obj.elem.places_list.find('a.place_in_list' + obj.active_marker).addClass('active');
        }
        else {
            if(no_active_marker) {
                obj.elem.places_list.find('a.place_in_list' + obj.active_marker).removeClass('active');
                obj.active_marker = null;
            }
            else {
                obj.elem.places_list.find('a.place_in_list' + obj.active_marker).addClass('active');
            }
        }
    }
};

ToGoList.prototype.initClickOnList = function() {
    var obj = this;

    obj.elem.places_list.on('click', 'a', function(e) {
        e.preventDefault();

        var $this = $(this);

        if(obj.active_marker) {
            obj.last_active_marker = obj.active_marker;
        }

        obj.active_marker = $this.data('id');

        obj.initActiveMarker(false, true);
    });
};

ToGoList.prototype.setPlaceVisited = function(id, visited) {
    var obj = this;

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/place/visited',
        data: {
            'id' : id,
            'visited' : visited
        },
        success: function(data) {
            if(data.success == 1) {
                obj.setMarkerColor(id, visited);
            }
        }
    });
};

ToGoList.prototype.setMarkerColor = function(id, visited) {
    var obj = this;

    if(typeof obj.markers[id] == 'undefined') {
        return false;
    }

    if(visited) {
        obj.markers[id].setIcon(obj.marker_color.visited);
    }
    else {
        obj.markers[id].setIcon(obj.marker_color.simple);
    }
};

ToGoList.prototype.initHelp = function() {
    var obj = this;

    obj.elem.new_places_icon.attr("src", obj.marker_color.simple);
    obj.elem.visited_places_icon.attr("src", obj.marker_color.visited);
};

$(function() {
    new ToGoList();
});