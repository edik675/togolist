<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/place/add', 'PlacesController@add')->name('add_place');
Route::post('/place/delete', 'PlacesController@delete')->name('delete_place');
Route::get('/places/get', 'PlacesController@get')->name('get_places');
Route::post('/place/visited', 'PlacesController@visited')->name('visited_place');
